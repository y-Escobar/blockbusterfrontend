import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from './company';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private urlEndPoint: string = "http://localhost:8090/companies";
  private urlEndPointCompany: string = "http://localhost:8090/company";
  private httpHeaders = new HttpHeaders({
    'Content-type': 'application/json'
  });

  constructor(private http: HttpClient) { }

  getCompanies(): Observable<Company[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Company[])
    )
  }

  postCompany(company: Company): Observable<Company> {
    return this.http.post<Company>(this.urlEndPointCompany, company, {headers: this.httpHeaders});
  }

  getCompany(id: number): Observable<Company> {
    return this.http.get<Company>(this.urlEndPointCompany + '/' + id).pipe(
      map(response => response as Company)
    );
  }

  putCompany(company: Company): Observable<Company> {
    return this.http.put<Company>(this.urlEndPointCompany + '/' + company.id, company, { headers: this.httpHeaders })
  }

  deleteCompany(id: number): Observable<Company> {
    return this.http.delete<Company>(this.urlEndPointCompany + "/" + id, { headers: this.httpHeaders });
  }
}
