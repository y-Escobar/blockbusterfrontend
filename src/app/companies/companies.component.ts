import { Component, OnInit } from '@angular/core';
import { Company } from './company';
import { CompanyService } from './company.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companies: Company[] = [];
  idOn: boolean = false;
  emptyList: boolean;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyService.getCompanies().subscribe(
      company => {
        this.companies = company;
        if (this.companies.length == 0) {
          this.emptyList = true;
        }
      }
    )
  }

  deleteCompany(company: Company): void {
    Swal.fire({
      title: `¿Deseas eliminar esta compañía?`,
      text: `${company.nombre}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, borrala'
    }).then((result) => {
      if (result.value) {
        this.companyService.deleteCompany(company.id).subscribe(
          response => {
            this.companies = this.companies.filter(cli => cli != company)
            Swal.fire(
              'Compañía eliminado',
              `La compañía ${company.nombre} ha sido eliminada`,
              'success'
            );
            if(this.companies.length == 0){
              this.emptyList = true;
            }
          }
        )

      }
    })
  }

}
