import { Component, OnInit } from '@angular/core';
import { Company } from '../company';
import { CompanyService } from '../company.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.css']
})
export class CompanyFormComponent implements OnInit {

  title: string = "Crear compañía";
  company: Company = new Company();

  constructor(private companyService: CompanyService, 
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadCompany();
  }

  postCompany(): void {
    this.companyService.postCompany(this.company).subscribe(
      company => {
        this.router.navigate(['/companies']);
        Swal.fire('Nueva compañía', this.company.nombre + ' creada con éxito', 'success');
      }
    );
  }

  putCompany(): void{
    this.companyService.putCompany(this.company).subscribe(
      company => {
        this.router.navigate(['/companies']);
        Swal.fire("Compañía actualizada", `Compañía ${this.company.nombre} actualizada con éxito`, "success")
      }
    )
  }

  loadCompany(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if(id){
          this.title = "Editar compañía";
          this.companyService.getCompany(id).subscribe(
            company => this.company = company
          );
        }
      } 
    )
  }

}
