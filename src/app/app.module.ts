import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ClientesComponent } from './clientes/clientes.component';
import { JuegosComponent } from './juegos/juegos.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { CompaniesComponent } from './companies/companies.component';
import { ClienteFormComponent } from './clientes/clientes-form/cliente-form.component';

import { ClienteService } from './clientes/cliente.service';
import { JuegoService } from './juegos/juego.service';
import { CompanyFormComponent } from './companies/company-form/company-form.component';
import { CompanyService } from './companies/company.service';
import { JuegoFormComponent } from './juegos/juego-form/juego-form.component';
import { TiendaFormComponent } from './tiendas/tienda-form/tienda-form.component';
import { TiendaService } from './tiendas/tienda.service';
import { StocksComponent } from './stocks/stocks.component';
import { StockFormComponent } from './stocks/stock-form/stock-form.component';
import { StockService } from './stocks/stock.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: 'juegos', component: JuegosComponent },
  { path: 'tiendas', component: TiendasComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'stocks', component: StocksComponent },
  { path: 'login', component: LoginComponent },
  { path: 'clientes/form', component: ClienteFormComponent },
  { path: 'clientes/form/:id', component: ClienteFormComponent },
  { path: 'companies/form', component: CompanyFormComponent },
  { path: 'companies/form/:id', component: CompanyFormComponent },
  { path: 'tiendas/form', component: TiendaFormComponent },
  { path: 'tiendas/form/:id', component: TiendaFormComponent },
  { path: 'juegos/form', component: JuegoFormComponent },
  { path: 'juegos/form/:id', component: JuegoFormComponent },
  { path: 'stocks/form', component: StockFormComponent },
  { path: 'stocks/form/:id', component: StockFormComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ClientesComponent,
    JuegosComponent,
    TiendasComponent,
    CompaniesComponent,
    ClienteFormComponent,
    CompanyFormComponent,
    JuegoFormComponent,
    TiendaFormComponent,
    StocksComponent,
    StockFormComponent,
    HomeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
  ],
  providers: [
    ClienteService,
    JuegoService,
    CompanyService,
    TiendaService,
    JuegoService,
    StockService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
