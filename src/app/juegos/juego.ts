import { Company } from '../companies/company';

export class Juego {

    id: number;
    titulo: String;
    fechaLanzamiento: String;
    precio: number;
    pegi: number;
    categoria: String;
    companyDTOs: Company[] = [];
    
}
