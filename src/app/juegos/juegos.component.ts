import { Component, OnInit } from '@angular/core';
import { JuegoService } from './juego.service';
import { Juego } from './juego';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {

  juegos: Juego[] = [];
  idOn: boolean = false;
  emptyList: boolean;

  constructor(private juegoService: JuegoService) { }

  ngOnInit(): void {
    this.juegoService.getJuegos().subscribe(
      juegos => {
        this.juegos = juegos;
        if (this.juegos.length==0) {
          this.emptyList = true;
        } 
      }
    )
  }

  deleteJuego(juego: Juego): void {
    Swal.fire({
      title: `¿Deseas eliminar este juego?`,
      text: `${juego.titulo}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, bórralo'
    }).then((result) => {
      if (result.value) {
        this.juegoService.deleteJuego(juego.id).subscribe(
          response => {
            this.juegos = this.juegos.filter(cli => cli != juego)
            Swal.fire(
              'Juego eliminado',
              `El juego ${juego.titulo} ha sido eliminado`,
              'success'
            );
            if(this.juegos.length == 0){
              this.emptyList = true;
            }
          }
        )
      }
    })
  }

}
