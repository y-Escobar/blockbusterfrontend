import { Injectable } from '@angular/core';
import { Juego } from './juego';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JuegoService {

  private urlEndPoint: string = "http://localhost:8090/juegos";

  httpHeaders: {
    'Content-type': 'application/json';
  }

  constructor(private http: HttpClient) { }

  getJuegos(): Observable<Juego[]>{
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Juego[]),
      catchError(error => {
        return throwError(error)
      })
    )
  }

  getJuego(id: number): Observable<Juego>{
    return this.http.get(this.urlEndPoint+"/"+id).pipe(
      map(response => response as Juego),
      catchError(error => {
        return throwError(error)
      })
    );
  }

  postJuego(juego: Juego): Observable<Juego> {
    return this.http.post<Juego>(this.urlEndPoint, juego, {headers: this.httpHeaders})
  }

  putJuego(juego: Juego): Observable<Juego>{
    return this.http.put<Juego>(this.urlEndPoint+"/"+juego.id, juego, {headers: this.httpHeaders})
  }

  deleteJuego(id: number): Observable<Juego> {
    return this.http.delete<Juego>(this.urlEndPoint+"/"+id, {headers: this.httpHeaders})
  }

}
