import { Component, OnInit } from '@angular/core';
import { Juego } from '../juego';
import { JuegoService } from '../juego.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Company } from 'src/app/companies/company';
import { CompanyService } from 'src/app/companies/company.service';

@Component({
  selector: 'app-juego-form',
  templateUrl: './juego-form.component.html',
  styleUrls: ['./juego-form.component.css']
})
export class JuegoFormComponent implements OnInit {

  title: string = "Añadir juego";
  juego: Juego = new Juego();
  companies: Company[] = [];

  constructor(private juegoService: JuegoService,
    private companyService: CompanyService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadJuego();
    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    )
  }

  postJuego(): void {
    this.juegoService.postJuego(this.juego).subscribe(
      juego => {
        this.router.navigate(["/juegos"]);
        Swal.fire("Juego añadido", `Juego ${this.juego.titulo} añadido con éxito`, "success");
      }
    )
  }

  putJuego(): void {
    this.juegoService.postJuego(this.juego).subscribe(
      juego => {
        this.router.navigate(["/juegos"]);
        Swal.fire("Juego actualizado", `Juego ${this.juego.titulo} actualizado con éxito`, "success");
      }
    )
  }

  loadJuego(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params["id"];
        if (id) {
          this.title = "Editar juego";
          this.juegoService.getJuego(id).subscribe(
            juego => this.juego = juego
          );
        }
      }
    )
  }



}
