import { Component, OnInit } from '@angular/core';
import { ClienteService } from './cliente.service';
import { Cliente } from './cliente';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[] = [];
  idOn: boolean = false;
  emptyList: boolean;

  constructor(private clienteService: ClienteService) { }

  ngOnInit(): void {
    this.clienteService.getClientes().subscribe(
      clientes => {
        this.clientes = clientes;
        if (this.clientes.length == 0) {
          this.emptyList = true;
        }
      }
    );
  }

  deleteCliente(cliente: Cliente): void {
    Swal.fire({
      title: `¿Deseas eliminar este cliente?`,
      text: `${cliente.nombre}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.clienteService.deleteCliente(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli != cliente)
            Swal.fire(
              'Cliente eliminado',
              `El cliente ${cliente.nombre} ha sido eliminado`,
              'success'
            );
            if(this.clientes.length == 0){
              this.emptyList = true;
            }
          }
        )

      }
    })
  }
}
