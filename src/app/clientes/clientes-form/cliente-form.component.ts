import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: []
})
export class ClienteFormComponent implements OnInit {

  cliente: Cliente = new Cliente();
  title: String = "Crear cliente";

  constructor(private clienteService: ClienteService, 
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadCliente();
  }

  postCliente(): void {
    this.clienteService.postCliente(this.cliente).subscribe(
      client => {
        this.router.navigate(['/clientes']);
        Swal.fire('Nuevo cliente', this.cliente.nombre + ' creado con éxito', 'success');
      }
    );
  }

  loadCliente(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if(id){
          this.title = "Editar cliente";
          this.clienteService.getCliente(id).subscribe(
            cliente => this.cliente = cliente
          );
        }
      } 
    )
  }

  putCliente(): void{
    this.clienteService.putCliente(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes']);
        Swal.fire("Cliente actualizado", `Cliente ${this.cliente.nombre} actualizado con éxito`, "success")
      }
    )
  }

}
