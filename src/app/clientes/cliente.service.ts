import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urlEndPoint = "http://localhost:8090/clientes";
  private urlEndPointCliente = "http://localhost:8090/cliente";

  private httpHeaders = new HttpHeaders({
    'Content-type': 'application/json'
  });

  constructor(private http : HttpClient) { }

  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.urlEndPoint).pipe(
      map(response => response as Cliente[]) 
    );
  }

  postCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.urlEndPointCliente, cliente, {headers: this.httpHeaders});
  }

  getCliente(id: number): Observable<Cliente>{
    return this.http.get<Cliente>(this.urlEndPointCliente+'/'+id).pipe(
      map(response => response as Cliente)
    );
  }

  putCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(this.urlEndPointCliente+'/'+cliente.id, cliente, {headers: this.httpHeaders})
  }

  deleteCliente(id: number): Observable<Cliente> {
    return this.http.delete<Cliente>(this.urlEndPointCliente+"/"+id , {headers: this.httpHeaders});
  }
}
