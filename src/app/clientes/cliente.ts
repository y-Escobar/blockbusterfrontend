export class Cliente {

    id: number;
    nombre: string;
    documentacion: string;
    username: string;
    password: string;
    correo: string;
    fechaNacimiento: string;

}
