import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tienda } from './tienda';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  private urlEndPoint: string = "http://localhost:8090/tiendas";
  private urlEndPointTienda: string = "http://localhost:8090/tienda";

  private httpHeaders = new HttpHeaders({
    'Content-type': 'application/json'
  });

  constructor(private http: HttpClient) { }

  getJuegos(): Observable<Tienda[]>{
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Tienda[])
    )
  }

  getTiendas(): Observable<Tienda[]> {
    return this.http.get<Tienda[]>(this.urlEndPoint).pipe(
      map(response => response as Tienda[]) 
    );
  }

  postTienda(tienda: Tienda): Observable<Tienda> {
    return this.http.post<Tienda>(this.urlEndPointTienda, tienda, {headers: this.httpHeaders});
  }

  getTienda(id: number): Observable<Tienda>{
    return this.http.get<Tienda>(this.urlEndPointTienda+'/'+id).pipe(
      map(response => response as Tienda)
    );
  }

  putTienda(tienda: Tienda): Observable<Tienda> {
    return this.http.put<Tienda>(this.urlEndPointTienda+'/'+tienda.id, tienda, {headers: this.httpHeaders})
  }

  deleteTienda(id: number): Observable<Tienda> {
    return this.http.delete<Tienda>(this.urlEndPointTienda+"/"+id , {headers: this.httpHeaders});
  }
}
