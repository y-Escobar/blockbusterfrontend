import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.css']
})
export class TiendasComponent implements OnInit {

  tiendas: Tienda[] = [];
  idOn: boolean = false;
  emptyList: boolean;

  constructor(private tiendaService: TiendaService) { }

  ngOnInit(): void {
    this.tiendaService.getJuegos().subscribe(
      tiendas => {
      this.tiendas = tiendas;
        if (this.tiendas.length == 0) {
          this.emptyList = true;
        }
      }
    )
  }

  deleteTienda(tienda: Tienda): void {
    Swal.fire({
      title: `¿Deseas eliminar esta tienda?`,
      text: `${tienda.nombre}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, bórrala'
    }).then((result) => {
      if (result.value) {
        this.tiendaService.deleteTienda(tienda.id).subscribe(
          response => {
            this.tiendas = this.tiendas.filter(cli => cli != tienda)
            Swal.fire(
              'Tienda eliminada',
              `La tienda ${tienda.nombre} ha sido eliminada`,
              'success'
            );
            if(this.tiendas.length == 0){
              this.emptyList = true;
            }
          }
        )

      }
    })
  }

}
