import { Component, OnInit } from '@angular/core';
import { Tienda } from '../tienda';
import { TiendaService } from '../tienda.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tienda-form',
  templateUrl: './tienda-form.component.html',
  styleUrls: ['./tienda-form.component.css']
})
export class TiendaFormComponent implements OnInit {

  title: string = "Añadir tienda";
  tienda: Tienda = new Tienda();

  constructor(private tiendaService: TiendaService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadTienda();
  }

  postTienda(): void {
    this.tiendaService.postTienda(this.tienda).subscribe(
      client => {
        this.router.navigate(['/tiendas']);
        Swal.fire('Nueva tienda', this.tienda.nombre + ' creada con éxito', 'success');
      }
    );
  }

  loadTienda(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if (id) {
          this.title = "Editar tienda";
          this.tiendaService.getTienda(id).subscribe(
            tienda => this.tienda = tienda
          );
        }
      }
    )
  }

  putTienda(): void {
    this.tiendaService.putTienda(this.tienda).subscribe(
      cliente => {
        this.router.navigate(['/tiendas']);
        Swal.fire("Tienda actualizada", `Cliente ${this.tienda.nombre} actualizada con éxito`, "success")
      }
    )
  }

}
