import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title: string = "BlockBuster";

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  checkLogin(): boolean{
    if(localStorage.getItem('authToken')!=null){
      return true;
    }
    else{
      return false;
    }
  }

  logOut(): void {
    localStorage.removeItem('authToken');
    this.router.navigate(['/home'])
  }

}
