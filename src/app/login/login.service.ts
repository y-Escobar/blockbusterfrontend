import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Credentials } from './credentials';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  constructor() { }

  save(credentials: Credentials): void {
    localStorage.setItem('authToken', btoa(`${credentials.username}:${credentials.password}`));
  }

  getAuthHeaders(): HttpHeaders {
    let token = localStorage.getItem('authToken');
    if (token) {
      return new HttpHeaders({'Authorization': 'Basic ' + token});
    }
    else {
      return null;
    }
  }
}
