import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Credentials } from './credentials';
import { ClienteService } from '../clientes/cliente.service';
import { Cliente } from '../clientes/cliente';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: Credentials = new Credentials();
  clientes: Cliente[] = [];

  constructor(private loginService: LoginService,
    private clienteService: ClienteService,
    private router: Router) { }

  ngOnInit(): void {
    this.clienteService.getClientes().subscribe(
      response => this.clientes = response
    )
  }

  saveLogin(): void {

    let valid: boolean = false;

    this.clientes.forEach(cliente => {
      if (cliente.username == this.credentials.username) {
        if (cliente.password == this.credentials.password) {
          this.loginService.save(this.credentials)
          valid = true;
          this.router.navigate(['/home'])
        }
      }
    });

    if (!valid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Usuario/Contraseña incorrecto',
        footer: '<a href>¿Has olvidado tu contraseña?</a>'
      })
    }

  }

}
