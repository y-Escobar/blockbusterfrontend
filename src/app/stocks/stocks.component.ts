import { Component, OnInit } from '@angular/core';
import { Stock } from './stock';
import { StockService } from './stock.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  idOn: boolean = false;
  stocks: Stock[] = [];
  emptyList: boolean;

  constructor(private stockService: StockService) { }

  ngOnInit(): void {
    this.stockService.getStocks().subscribe(
      stocks => {
        this.stocks = stocks;
        if(stocks.length==0){
          this.emptyList = true;
        }
      }
    )
  }

  deleteStock(stock: Stock): void {
    Swal.fire({
      title: `¿Deseas eliminar este stock?`,
      text: `${stock.referencia}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, bórralo'
    }).then((result) => {
      if (result.value) {
        this.stockService.deleteStock(stock.id).subscribe(
          response => {
            this.stocks = this.stocks.filter(cli => cli != stock)
            Swal.fire(
              'Stock eliminado',
              `El stock ${stock.referencia} ha sido eliminado`,
              'success'
            );
            if(this.stocks.length == 0){
              this.emptyList = true;
            }
          }
        )
      }
    })
  }

}
