import { Component, OnInit } from '@angular/core';
import { Stock } from '../stock';
import { Cliente } from 'src/app/clientes/cliente';
import { Tienda } from 'src/app/tiendas/tienda';
import { Juego } from 'src/app/juegos/juego';
import { JuegoService } from 'src/app/juegos/juego.service';
import { StockService } from '../stock.service';
import { ClienteService } from 'src/app/clientes/cliente.service';
import { TiendaService } from 'src/app/tiendas/tienda.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-stock-form',
  templateUrl: './stock-form.component.html',
  styleUrls: ['./stock-form.component.css']
})
export class StockFormComponent implements OnInit {

  title: string = "Añadir stock";
  stock: Stock = new Stock();
  cliente: Cliente;
  juego: Juego;
  tienda: Tienda;
  clientes: Cliente[] = [];
  juegos: Juego[] = [];
  tiendas: Tienda[] = [];



  constructor(
    private sS: StockService,
    private cS: ClienteService,
    private jS: JuegoService,
    private tS: TiendaService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    enum status {
      VENDIDO, ALQUILADO, TIENDA
    }
  }

  ngOnInit(): void {
    this.loadStock();
    this.cS.getClientes().subscribe(
      clientes => this.clientes = clientes
    )
    this.jS.getJuegos().subscribe(
      juegos => this.juegos = juegos
    )
    this.tS.getTiendas().subscribe(
      tiendas => this.tiendas = tiendas
    )

  }

  loadStock(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params["id"];
        if (id) {
          this.title = "Editar juego";
          this.sS.getStock(id).subscribe(
            juego => this.stock = juego
          );
        }
      }
    )
  }

  postStock(): void {
    this.sS.postStock(this.stock).subscribe(
      stock => {
        this.router.navigate(["/stocks"]);
        Swal.fire("Stock añadido", `Stock ${this.stock.referencia} añadido con éxito`, "success");
      }
    )
  }

  putStock(): void {
    this.sS.putStock(this.stock).subscribe(
      stock => {
        this.router.navigate(["/stocks"]);
        Swal.fire("Stock actualizado", `Stock ${this.stock.referencia} actualizado con éxito`, "success");
      }
    )
  }
}
