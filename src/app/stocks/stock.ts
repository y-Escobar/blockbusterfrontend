import { Juego } from '../juegos/juego';
import { Tienda } from '../tiendas/tienda';
import { Cliente } from '../clientes/cliente';

export class Stock {

    id: number;
    referencia: string;
    estado: string;
    juegoDTO: Juego;
    tiendaDTO: Tienda;
    clienteDTO: Cliente;

}
