import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stock } from './stock';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  private urlEndPoint: string = "http://localhost:8090/stocks";

  httpHeaders: {
    'Content-type': 'application/json';
  }

  constructor(private http: HttpClient) { }

  getStocks(): Observable<Stock[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(
        response => response as Stock[]
      )
    )
  }

  getStock(id: number): Observable<Stock> {
    return this.http.get(this.urlEndPoint + "/" + id).pipe(
      map(
        response => response as Stock
      )
    )
  }

  postStock(stock: Stock): Observable<Stock>{
    return this.http.post<Stock>(this.urlEndPoint, stock, {headers: this.httpHeaders})
  }

  putStock(stock: Stock): Observable<Stock>{
    return this.http.put<Stock>(this.urlEndPoint+"/"+stock.id, stock, {headers: this.httpHeaders});
  }

  deleteStock(id: number): Observable<Stock>{
    return this.http.delete<Stock>(this.urlEndPoint+"/"+id, {headers: this.httpHeaders})
  }
}
